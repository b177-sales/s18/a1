let trainerCard = {
	name: 'Ash Ketchum',
	age: 18,
	pokemon: ['Lucario', 'Pikachu', 'Dragonite', 'Gengar', 'Mega Charizard'],
	friends: {
		hoenn: ['May','Max'],
		kanto: ['Brock', 'Misty'],
	},
	talk: function(){
		console.log('Lucario' + '! I choose you.')
	}
}

console.log(trainerCard);
console.log('Result of dot notation:');
console.log(trainerCard.name);

console.log('Result of square bracket notation:');
console.log(trainerCard['pokemon']);

console.log('Result of talk notation:');
trainerCard.talk();


function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.skill = function(target){
		console.log(this.name + ' used Dragon Outrage on ' + target.name + '.');
		newhealth = (target.health - this.attack);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		if(target.health - this.attack){
			target.health = newhealth
		}
		if(newhealth <= 0){
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
}

let lucario = new Pokemon('Lucario', 48);
let palkia = new Pokemon('Palkia', 45);
let zekrom = new Pokemon('Zekrom', 100);


console.log(lucario)
console.log(palkia)
console.log(zekrom)

palkia.skill(lucario)
console.log(lucario)

zekrom.skill(palkia)
console.log(palkia)


/*let workAddress = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}
console.log("Work Address: ");
console.log(workAddress);*/